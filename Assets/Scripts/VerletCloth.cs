﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeighborConstraint
{
    static float restDist = 1.5f;
    static float limit;
    static float stiffness;
    public int pm1_index;
    public int pm2_index;

    public void Solve(PointMass pm1, PointMass pm2, string which, Line currentLine, float tearLimit) //Changed so that it solves per pm instead of looping over array
    {
        // calculate difference
        Vector3 difference = pm1.position - pm2.position;
        float diffMag = (difference).magnitude;
        float diffScalar = (restDist - diffMag) / diffMag;

        //check if difference is larger than certain treshold
        // delete corresponding constraint and line if this is the case
        limit = tearLimit * restDist; 
        if (diffMag > limit)
        {
            Debug.Log("diff mag: " + diffMag);
            Debug.Log("limit " + limit);
            if (which == "left")
            {
                pm1.left = null;
                currentLine.pm2 = pm1;
            }
               
            if (which == "upper")
            {
                pm1.upper = null;
                currentLine.pm2 = pm1;
            }
        }

        //Update position whilst taking stiffness into account
        stiffness = 1.0f; 
        float massInverse1 = 1 / pm1.mass;
        float massInverse2 = 1 / pm2.mass;
        float massScalar1 = (massInverse1 / (massInverse1 + massInverse2)) * stiffness;
        float massScalar2 = stiffness - massScalar1;

        Vector3 translation1 = difference * massScalar1 * diffScalar;
        Vector3 translation2 = difference * massScalar2 * diffScalar;
        pm1.position += translation1;
        pm2.position -= translation2;
    }

    public (Vector3, Vector3) GetVelocityFromConstraint()
    {
        // calculate difference
        PointMass pm1 = VerletCloth.Singleton.pointMasses[pm1_index];
        PointMass pm2 = VerletCloth.Singleton.pointMasses[pm2_index];


        Vector3 difference = pm1.position - pm2.position;
        float diffMag = (difference).magnitude;
        float diffScalar = (restDist - diffMag) / diffMag;

        //Update position whilst taking stiffness into account
        stiffness = 1.0f;
        float massInverse1 = 1 / pm1.mass;
        float massInverse2 = 1 / pm2.mass;
        float massScalar1 = (massInverse1 / (massInverse1 + massInverse2)) * stiffness;
        float massScalar2 = stiffness - massScalar1;

        //Vector3 translation = difference * 0.5f * diffScalar;
        //pm1.position += translation;
        //pm2.position -= translation;
        Vector3 translation1 = difference * massScalar1 * diffScalar;
        Vector3 translation2 = difference * massScalar2 * diffScalar;

        return (translation1, translation2);
    }
}

public class StickConstraint
{
    public int pm_index;
    public Vector3 stickPosition;
    public string type;
    GameObject left, right;
    float currentLeft, currentRight, adjustRight;

    public void Update()
    {
        left = GameObject.Find("Catcher/LeftHandle");
        currentLeft = left.transform.position.x;
        right = GameObject.Find("Catcher/RightHandle");
        currentRight = right.transform.position.x;

        if (type == "left") stickPosition.x = currentLeft;
        if (type == "right") stickPosition.x = currentRight;
    }

    public void Solve(ref PointMass[] pointMasses)
    {
        pointMasses[pm_index].position = stickPosition;
    }
}

public class Line
{
    public PointMass pm1;
    public PointMass pm2;
    public GameObject clothLink = new GameObject();
    public LineRenderer line;

    public void Update(float dt)
    {
        line.SetPosition(0, pm1.position);
        line.SetPosition(1, pm2.position);
    }
}

public class PointMass
{
    public Vector3 position;
    public NeighborConstraint left, upper;
    public Line leftLine, upperLine;
    public float mass = 100;
    public Vector3 velocity;
    public bool shouldApplyConstraint = true; // set to false when offsetted by collision

    Vector3 prevPosition;

    //static Vector3 acc = new Vector3(0.0f, -9.8f, 0.0f);
    static Vector3 acc = new Vector3(0.0f, 0.0f, 0.0f);

    public PointMass(Vector3 initialPosition)
    {
        position = initialPosition;
        prevPosition = position;
    }

    public void Update(float dt, ref PointMass[] pointMasses, int constraintIterations, float tearLimit)
    {
        if (!shouldApplyConstraint)
        {
            // reset and return
            shouldApplyConstraint = true;
            return;
        }

        // Solve neighbor constraints
        for (int i = 0; i < constraintIterations; i++)
        {
            if (upper != null)
            {
                upper.Solve(pointMasses[upper.pm1_index], pointMasses[upper.pm2_index], "upper", upperLine, tearLimit);
                upperLine.Update(Time.deltaTime);
            }
               
            if (left != null)
            {
                left.Solve(pointMasses[left.pm1_index], pointMasses[left.pm2_index], "left", leftLine, tearLimit);
                leftLine.Update(Time.deltaTime);
            }
        }

        // Update pointmass position
        velocity = (position - prevPosition) / dt;
        Vector3 nextPosition = position + velocity * dt + acc * dt;
        prevPosition = position;
        position = nextPosition;
    }
}

public class VerletCloth : MonoBehaviour
{
    public Material material;
    public Material transpMaterial;

    // NOTE: better to change these variables in the unity editor on the Cloth gameObject
    public float clothGridCellSize = 1.5f; // if you change this, also change rest Dist in neighbor constraint class
    public int clothGridWidth = 15, clothGridHeight = 15;
    public float tearLimit = 60f;

    public PointMass[] pointMasses;
    NeighborConstraint[] neighborConstraints;
    StickConstraint[] stickConstraints;
    Line[] lines;
    MeshFilter clothMesh;
    MeshCollider clothCollider;
    Vector3[] positions;

    public static VerletCloth Singleton;
    GameObject leftHandle, rightHandle;
    float startLeft, startRight;

    const int numConstraintSolves = 5;
    int constraint_index;

    private void Awake()
    {
        if(!Singleton)
        {
            Singleton = this;
        }
        else if (Singleton != this)
        {
            Destroy(this);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        constraint_index = 0;
        transpMaterial = Resources.Load("ClothTransparent", typeof(Material)) as Material;
        leftHandle = GameObject.Find("Catcher/LeftHandle");
        startLeft = leftHandle.transform.position.x;
        rightHandle = GameObject.Find("Catcher/RightHandle");
        startRight = rightHandle.transform.position.x;

        GeneratePointMasses(); // Containts setting up neighborconstraints and lines
        GenerateStickConstraints();
        SetupMesh();
        positions = new Vector3[clothMesh.mesh.vertices.Length];
    }

    float lastTime, leftOverTime;
    // Update is called once per frame
    void Update()
    {
        clothCollider.sharedMesh = clothMesh.mesh;
        // solve constraints
        for (int i = 0; i < numConstraintSolves; i++)
        {
            foreach (StickConstraint sc in stickConstraints)
            {
                sc.Update();
                sc.Solve(ref pointMasses);
            }
        }

       // Debug.Log("Updating point mass....");
        // update point masses
        for (int i = 0; i < pointMasses.Length; i++)
        {
            pointMasses[i].Update(Time.deltaTime, ref pointMasses, numConstraintSolves, tearLimit);
            positions[i] = pointMasses[i].position; // immediately update vertices
            clothMesh.mesh.vertices = positions;
            clothMesh.mesh.RecalculateNormals();
        }
    }

    void GeneratePointMasses()
    {
        int pointWidth = clothGridWidth + 1;
        int pointHeight = clothGridHeight + 1;
        pointMasses = new PointMass[pointWidth * pointHeight];

        float leftOffset = leftHandle.transform.position.x;
        float forwardOffset = (clothGridHeight * clothGridCellSize) / 2;

        // define left->right, bottom->top
        for (int y = 0; y < pointHeight; y++)
        {
            for (int x = 0; x < pointWidth; x++)
            {
                pointMasses[pointWidth * y + x] = new PointMass(new Vector3(x * clothGridCellSize + leftOffset, 0.0f, (pointHeight - 1 - y) * clothGridCellSize - forwardOffset)); // include offset
                int vertexIndex = y * (clothGridWidth + 1) + x;

                if (x == 0 && y == 0) continue;

                else if (x == 0 && y > 0)
                {
                    // only set upper vertex neighbor constraint
                    pointMasses[vertexIndex].upper = new NeighborConstraint();
                    pointMasses[vertexIndex].upper.pm1_index = vertexIndex;
                    pointMasses[vertexIndex].upper.pm2_index = vertexIndex - (clothGridWidth + 1);
                    pointMasses[vertexIndex].upperLine = new Line();
                    CreateNewLine(pointMasses[vertexIndex].upperLine, pointMasses[vertexIndex], pointMasses[vertexIndex - (clothGridWidth + 1)]);
                    constraint_index++;
                    continue;
                }

                else if (y == 0 && x > 0)
                {
                    // only set left vertex neighbor constraint
                    pointMasses[vertexIndex].left = new NeighborConstraint();
                    pointMasses[vertexIndex].left.pm1_index = vertexIndex;
                    pointMasses[vertexIndex].left.pm2_index = vertexIndex - 1;
                    pointMasses[vertexIndex].leftLine = new Line();
                    CreateNewLine(pointMasses[vertexIndex].leftLine, pointMasses[vertexIndex], pointMasses[vertexIndex - 1]);
                    constraint_index++;
                    continue;
                }

                else
                {
                    // left neighbor
                    pointMasses[vertexIndex].left = new NeighborConstraint();
                    pointMasses[vertexIndex].left.pm1_index = vertexIndex;
                    pointMasses[vertexIndex].left.pm2_index = vertexIndex - 1;
                    pointMasses[vertexIndex].leftLine = new Line();
                    CreateNewLine(pointMasses[vertexIndex].leftLine, pointMasses[vertexIndex], pointMasses[vertexIndex - 1]);
                    constraint_index++;
                    // upper neighbor
                    pointMasses[vertexIndex].upper = new NeighborConstraint();
                    pointMasses[vertexIndex].upper.pm1_index = vertexIndex;
                    pointMasses[vertexIndex].upper.pm2_index = vertexIndex - (clothGridWidth + 1);
                    pointMasses[vertexIndex].upperLine = new Line();
                    CreateNewLine(pointMasses[vertexIndex].upperLine, pointMasses[vertexIndex], pointMasses[vertexIndex - (clothGridWidth + 1)]);
                    constraint_index++;
                }
            }
        }
    }

    void GenerateStickConstraints() // Left and right side attached to handlers
    {
        stickConstraints = new StickConstraint[(clothGridWidth + 1) * 2];
        int cIndex = 0;
        //float adjustRight = leftHandle.transform.position.x + rightHandle.transform.position.x;
        float adjustRight = (rightHandle.transform.position.x - (clothGridWidth * clothGridCellSize)) - leftHandle.transform.position.x;

        // for left side (position.x == 0)
        for (int i = 0; i < pointMasses.Length; i = i + clothGridWidth + 1)
        {
            stickConstraints[cIndex] = new StickConstraint();
            stickConstraints[cIndex].pm_index = i;
            stickConstraints[cIndex].stickPosition = pointMasses[i].position;
            stickConstraints[cIndex].type = "left";
            cIndex++;
        }

        // for right side (position.x == clothGridWidth)
        for (int i = clothGridWidth; i < pointMasses.Length; i = i + clothGridWidth + 1)
        {
            stickConstraints[cIndex] = new StickConstraint();
            stickConstraints[cIndex].pm_index = i;
            stickConstraints[cIndex].stickPosition = pointMasses[i].position + new Vector3(adjustRight, 0, 0);
            stickConstraints[cIndex].type = "right";
            cIndex++;
        }
    }

    public void CreateNewLine(Line currentLine, PointMass pm1, PointMass pm2)
    {
        currentLine.pm1 = pm1;
        currentLine.pm2 = pm2;
        currentLine.clothLink.transform.position = pm1.position;
        currentLine.clothLink.AddComponent<LineRenderer>();
        currentLine.line = currentLine.clothLink.GetComponent<LineRenderer>();
        currentLine.line.GetComponent<LineRenderer>().material = material;
        currentLine.line.SetWidth(0.1f, 0.1f);
        currentLine.line.SetPosition(0, pm1.position);
        currentLine.line.SetPosition(1, pm2.position);
    }

    void SetupMesh()
    {
        Vector3[] vertices = new Vector3[pointMasses.Length];
        for (int i = 0; i < pointMasses.Length; i++) vertices[i] = pointMasses[i].position;

        Vector2[] uvs = new Vector2[vertices.Length];
        for (int y = 0; y < clothGridHeight + 1; y++)
        {
            for (int x = 0; x < clothGridWidth + 1; x++)
            {
                uvs[y * (clothGridWidth + 1) + x] = new Vector2(x / (float)clothGridWidth, y / (float)clothGridHeight);
            }
        }

        int[] triangles = new int[clothGridHeight * clothGridWidth * 2 * 3]; // dimensions * triangles per grid cell * vertices per triangle
        // define left->right, top->bottom
        for (int y = 0; y < clothGridHeight; y++)
        {
            for (int x = 0; x < clothGridWidth; x++)
            {
                int triOffset = clothGridWidth * y * 6 + x * 6;
                int indexOffset = (clothGridWidth + 1) * y + x;

                // define in clockwise order!
                triangles[triOffset + 0] = indexOffset;
                triangles[triOffset + 1] = indexOffset + 1;
                triangles[triOffset + 2] = indexOffset + clothGridWidth + 1;
                triangles[triOffset + 3] = indexOffset + clothGridWidth + 1;
                triangles[triOffset + 4] = indexOffset + 1;
                triangles[triOffset + 5] = indexOffset + clothGridWidth + 2;
            }
        }

        Mesh mesh = new Mesh();

        mesh.vertices = vertices;
        mesh.uv = uvs;
        mesh.triangles = triangles;

        mesh.RecalculateNormals();

        GameObject cloth = new GameObject("Mesh", typeof(MeshFilter), typeof(MeshRenderer), typeof(MeshCollider));

        clothMesh = cloth.GetComponent<MeshFilter>();
        clothMesh.mesh = mesh;
        cloth.GetComponent<MeshRenderer>().material = transpMaterial;
        clothCollider = cloth.GetComponent<MeshCollider>();
        clothCollider.cookingOptions = MeshColliderCookingOptions.None;

        // setup right orientation / position
        cloth.transform.localScale = new Vector3(1.0f, 1.0f, -1.0f);
        Bounds b = clothMesh.mesh.bounds;

        cloth.transform.Translate(new Vector3(0.0f, 0.0f, 0.0f));
    }

    // resolves collision at a given position and returns the new impulse velocity for the mass object
    //public Vector3 ResolveCollision(float objWeight, Vector3 objAcc, Vector3 collPosition, Vector3 collNormal, float collDepth)
    public (Vector3, Vector3) ResolveCollision(float objMass, Vector3 objAcc, Vector3 objVel, Vector3 objPos, Vector3 collPosition, Vector3 collNormal, float collDepth, bool firstColl)
    {
        // find four point masses that surround collposition (O(n)...)
        PointMass[] pointsToOffset = new PointMass[4];
        const float influenctialPointSquareRadius = 1.5f;
        List<PointMass> influentialPoints = new List<PointMass>(); // point masses that will influence that force against the mass object
        Vector3 newVelObject = new Vector3(0.0f, 0.0f, 0.0f);

        for (int y = 0; y < clothGridHeight + 1; y++)
        {
            int heightOffset = y * (clothGridWidth + 1);
            for (int x = 0; x < clothGridWidth + 1; x++)
            {
                int index = heightOffset + x;

                Vector3 pos = pointMasses[index].position;
                // ignore the y-value when calculating the distance
                Vector2 pos2d = new Vector2(pos.x, pos.z); // new Vector2(pointMasses[index].position.x, pointMasses[index].position.z)
                Vector2 coll2d = new Vector2(collPosition.x, collPosition.z);

                //  if (collPosition.x < pos.x && collPosition.y < pos.y)
                // Only take the four closest PM and apply new velocities to them
                if (coll2d.x < pos2d.x && coll2d.y > pos2d.y && pointMasses[index].upper != null && pointMasses[index].left != null
                    && coll2d.x > pointMasses[index - (clothGridWidth + 1) - 1].position.x && coll2d.y < pointMasses[index - (clothGridWidth + 1) - 1].position.z)
                {
                    influentialPoints.Clear();
                    influentialPoints.Add(pointMasses[index]); // point itself
                    //Debug.Log("Point itself: " + pointMasses[index].position);
                    influentialPoints.Add(pointMasses[index - 1]); // left neighbor
                    //Debug.Log("Point left: " + pointMasses[index - 1].position);
                    influentialPoints.Add(pointMasses[index - (clothGridWidth + 1)]);  // upper neighbor
                    //Debug.Log("Point up: " + pointMasses[index - (clothGridWidth + 1)].position);
                    influentialPoints.Add(pointMasses[index - (clothGridWidth + 1) - 1]); // upperleft neighbor
                    //Debug.Log("Point upleft: " + pointMasses[index - (clothGridWidth + 1) - 1].position);

                    // subtract both forces and calculate new force
                    Vector3 totalClothVelocity = new Vector3();
                    float totalClothMass = 0.0f;

                    // average over the four pm
                    totalClothMass = totalClothMass / 4;
                    totalClothVelocity = totalClothVelocity / 4;
                }
            }


        }

        Vector3 tentVelocity = new Vector3();
        Vector3 prevVelocity = new Vector3();
        foreach (PointMass pm in influentialPoints)
        {
            Vector3 pm1_vel1, pm1_vel2;
            (pm1_vel1, _) = pm.left.GetVelocityFromConstraint();
            tentVelocity += pm1_vel1;

            (pm1_vel2, _) = pm.upper.GetVelocityFromConstraint();
            tentVelocity += pm1_vel2;

            prevVelocity += pm.velocity;

        }

        tentVelocity /= (float)influentialPoints.Count;
        prevVelocity /= (float)influentialPoints.Count;

        if(tentVelocity.y < 0.0 && !firstColl)
        {
            return (Vector3.zero, prevVelocity);
        }

        Vector3 acceleration = tentVelocity - prevVelocity;

        const float springConstant = 0.01f;
        Vector3 clothForce = acceleration * springConstant;

        Vector3 newObjForce = new Vector3(0.0f, -9.8f, 0.0f) * objMass + clothForce;
        Vector3 newObjAcc = newObjForce / objMass;
        Vector3 newSpherePos = new Vector3();
        Vector3 newObjVel = objVel + newObjAcc * Time.deltaTime;
        newObjVel.x = 0.0f;
        newObjVel.z = 0.0f;

        foreach (PointMass pm in influentialPoints)
        {
            pm.shouldApplyConstraint = false;
            Vector3 posUpdate = newObjVel * Time.deltaTime;

            pm.position += posUpdate;
            newSpherePos = objPos + posUpdate;
        }

        return (newSpherePos, newObjVel);
    }


    void SetUpLines()
    {
        lines = new Line[constraint_index];
        int index = 0;
        foreach (PointMass pm1 in pointMasses)
        {
            if (pm1.left != null)
            {
                lines[index] = new Line();
                CreateNewLine(lines[index], pm1, pointMasses[pm1.left.pm2_index]);
                index++;
            }

            if (pm1.upper != null)
            {
                lines[index] = new Line();
                CreateNewLine(lines[index], pm1, pointMasses[pm1.upper.pm2_index]);
                index++;
            }
        }
    }
}
