﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClothHandle : MonoBehaviour
{
    [SerializeField]
    private bool isLeftHandle;

    [SerializeField]
    private float speed;

    private KeyCode leftKey, rightKey;

    // Start is called before the first frame update
    void Start()
    {
        if(isLeftHandle)
        {
            leftKey = KeyCode.A;
            rightKey = KeyCode.D;
        }
        else
        {
            leftKey = KeyCode.LeftArrow;
            rightKey = KeyCode.RightArrow;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(leftKey))
        {
            Vector3 newPos = transform.position;
            newPos.x -= speed * Time.deltaTime;

            transform.position = newPos;
        }
        if(Input.GetKey(rightKey))
        {
            Vector3 newPos = transform.position;
            newPos.x += speed * Time.deltaTime;

            transform.position = newPos;
        }
    }
}
