﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MassObject : MonoBehaviour
{
    [SerializeField]
    private float mass;

    [SerializeField]
    private float fallSpeed;

    [SerializeField]
    private float minRotImpulse;

    [SerializeField]
    private float maxRotImpulse;

    Vector3 angularRotation;
    float angularRotationSpeed;
    const float yPosKillThreshold = -20.0f;
    Rigidbody rb;

    Vector3 velocity, lastVelocity, acceleration;
    Vector3 gravity;
    Vector3 impulse;

    Vector3 newAcceleration;
    Vector3 gluedPosition;

    bool updateStatic = false;
    bool isStatic = false;
    bool didCollide = false;

    static int counter = 0;

    private void Start()
    {
        angularRotation = Random.rotation.eulerAngles;
        angularRotationSpeed = Random.Range(minRotImpulse, maxRotImpulse);

        gravity = new Vector3(0.0f, -fallSpeed, 0.0f);

        GetComponent<Rigidbody>().AddTorque(angularRotation * angularRotationSpeed);

        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if(isStatic && !didCollide)
        {
            isStatic = false;
        }
        if (transform.position.y < yPosKillThreshold)
        {
            Destroy(gameObject);
        }

        if(impulse.sqrMagnitude > 0.0f)
        {
            velocity = impulse;
        }

        if(updateStatic)
        {
            transform.position = gluedPosition;
            updateStatic = false;
            return;
        }

        acceleration = gravity;
        velocity += acceleration * Time.deltaTime;
        lastVelocity = velocity;

        Vector3 newPos = transform.position + velocity * Time.deltaTime;
        transform.position = newPos;

        impulse = Vector3.zero;
        newAcceleration = Vector3.zero;
        didCollide = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.CompareTag("NonCloth"))
        {
            return;
        }
        // get all contact points
        ContactPoint[] points = new ContactPoint[collision.contactCount];
        collision.GetContacts(points);
        
        Vector3 newVel = new Vector3();
        // for each contact point, resolve collisions
        foreach(ContactPoint cp in points)
        {
            (gluedPosition, velocity) = VerletCloth.Singleton.ResolveCollision(mass, acceleration, velocity, transform.position, cp.point, cp.normal, cp.separation, !isStatic);

            if (!isStatic)
            {
                isStatic = true;
            }
        }

        if (gluedPosition.sqrMagnitude > 0)
        {
            updateStatic = true;
        }

        didCollide = true;
    }
}
