﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectManager : MonoBehaviour
{
    [SerializeField]
    private GameObject[] objectPrefabs;

    [SerializeField]
    private float spawnYLocation = 40;

    [SerializeField]
    private Vector2 spawnXRange;

    [SerializeField]
    private Vector2 randomIntervalRange;

    private int numPrefabs;
    private float timer;
    private float zOffset = 0.1f;
    private Vector2 spawnZRange = new Vector2(-5f, 5f);

    // Start is called before the first frame update
    void Start()
    {
        numPrefabs = objectPrefabs.Length;

        // spawn initial prefab
        //SpawnObject();
        timer = GetNextSpawnTime();
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;

        if(timer < 0.0f)
        {
            SpawnObject();
            timer = GetNextSpawnTime();
        }
    }

    private void SpawnObject()
    {
        int randomIndex = (int)(Random.Range(0.0f, (float)numPrefabs));
        while (zOffset == 0.0f) zOffset = Random.Range(spawnZRange[0], spawnZRange[1]);
        Vector3 spawnPos = new Vector3(Random.Range(spawnXRange[0], spawnXRange[1]), spawnYLocation, zOffset);
        zOffset = 0.0f;
        Instantiate(objectPrefabs[randomIndex], spawnPos, Quaternion.identity);
    }

    private float GetNextSpawnTime()
    {
        return Random.Range(randomIntervalRange[0], randomIntervalRange[1]);
    }
}
